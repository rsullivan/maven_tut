package com.hello.maven.new_maven_project;

public class Harrier extends Airplane {
	
	private Engine jet;
	
	public Harrier(Engine jet) {
		
		 this.jet = jet;
	}
	

	@Override
	public Boolean hasWings() {
		// TODO Auto-generated method stub
		return super.hasWings();
	}

	@Override
	public Boolean hasEngine() {
		// TODO Auto-generated method stub
		return super.hasEngine();
	}

	@Override
	public String takesOff() {
		// TODO Auto-generated method stub
		return super.takesOff();
	}

	@Override
	public String engineType() {
		// TODO Auto-generated method stub
		return jet.getEngineType();
	}
	
	

}
