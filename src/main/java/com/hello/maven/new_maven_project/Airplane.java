package com.hello.maven.new_maven_project;

public class Airplane {
	
	private Engine engine;
	
	public Airplane(){}
	
	public Airplane(Engine engine){
		
		//Airplane has to be dependent on Engine
		this.engine = engine;
	}

	public Boolean hasWings() {
		// TODO Auto-generated method stub
		return true;
	}

	public Boolean hasEngine() {
		// TODO Auto-generated method stub
		return true;
	}

	public String takesOff() {
		// TODO Auto-generated method stub
		return "horizontal";
	}

	public String engineType() {
		
		return engine.getEngineType();
	}

	public void hasEngineType(String string) {
		
		
	}

}
