package com.hello.maven.new_maven_project;

import static org.junit.Assert.*;

import org.junit.Test;

public class AirplaneTest {

	@Test
	public void hasWings() {
		
		Airplane classUnderTest = new Airplane();
		Boolean expectedAnswer = true;
		Boolean actualAnswer = classUnderTest.hasWings();
		assertEquals(expectedAnswer, actualAnswer);
	}
	
	@Test
	public void hasEngine(){
		
		Airplane classUnderTest = new Airplane();
		Boolean expectedAnswer = true;
		Boolean actualAnswer = classUnderTest.hasEngine();
		assertEquals(expectedAnswer, actualAnswer);
		
	}
	
	@Test
	public void takesOff(){
		Airplane classUnderTest = new Airplane();
		String expectedAnswer = "horizontal";
		String actualAnswer = classUnderTest.takesOff();
		assertEquals(expectedAnswer, actualAnswer);
	}
	
	@Test
	public void JetPlaneHasWingsToo(){
		
		Airplane jet = new Jet();
		Boolean expectedAnswer = true;
		Boolean actualAnswer = jet.hasWings();
		assertEquals(expectedAnswer, actualAnswer);
		
	}
	
	@Test
	public void JetHasEngine(){
		Airplane jet = new Jet();
		Boolean expectedAnswer = true;
		Boolean actualAnswer = jet.hasEngine();
		assertEquals(expectedAnswer, actualAnswer);
		
	}
	
	@Test
	public void JetTakesOff(){
		
		Airplane jet = new Jet();
		String expectedAnswer = "fast and loud";
		String actualAnswer = jet.takesOff();
		assertEquals(expectedAnswer, actualAnswer);
		
	}
	
	@Test
	public void typeOfEngine(){
		
		
		Engine jet = new Engine();
		jet.setEngineType("jet");
		Airplane harrier = new Harrier(jet);
		String expectedAnswer = "This is a jet engine";
		String actualAnswer = harrier.engineType();
		assertEquals(expectedAnswer, actualAnswer);
		
	}
	
	@Test
	public void propEngineCraft(){
		
		Engine prop = new Engine();
		prop.setEngineType("turbo prop");
		Airplane lockheed = new Airplane(prop);
		String expectedAnswer = "This is a turbo prop engine";
		String actualAnswer = lockheed.engineType();
		assertEquals(expectedAnswer, actualAnswer);
		
	}
	
	@Test
	public void noEngine(){
		
		Airplane glider = new Airplane();
		
		
	}

}
